from django.test import TestCase

# Create your tests here.
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index

# Create your tests here.
class Story9Test(TestCase):
    def test_story9_url(self):
        response = Client().get('/story9/')
        self.assertEqual(response.status_code, 200)

    def test_using_story9_template_success(self):
        response = Client().get('/story9/')
        self.assertTemplateUsed(response, 'story9/index.html')

    def test_using_story9_template_success(self):
        response = Client().get('/story9/register/')
        self.assertTemplateUsed(response, 'registration/register.html')

    # def test_using_index_function(self):
    #     found = resolve('/story9/')
    #     self.assertEqual(found.func, index)


