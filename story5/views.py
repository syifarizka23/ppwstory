from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
from .models import Course
from .forms import Input_Form
from django.urls import reverse

# Create your views here.
def matakuliah(request):
    courses = Course.objects.all()
    response = {'courses' : courses, 'input_form' : Input_Form}
    return render(request, 'story5/courses.html', response)


def savematakuliah(request):
    form = Input_Form(request.POST or None)
    if (form.is_valid() and request.method == 'POST'):
        form.save()
        return HttpResponseRedirect(reverse('story5:matakuliah'))
    else:
        form = Input_Form()
        validate = {'validate' : 'SKS berentang 1 - 24', 'input_form':form}  
        return render(request,'story5/courses.html', validate)

def getmatkul(request, pk):
    course_instance = get_object_or_404(Course, pk=pk)
    response = {'course_instance' : course_instance}
    return render(request, 'story5/getmatkul.html', response)

def removematkul(request, pk):
    course_instance = get_object_or_404(Course, pk=pk)
    response = {'course_instance' : course_instance}
    if request.method== "POST":
        course_instance.delete()
        return render(request, 'story5/removematkul.html', response)

