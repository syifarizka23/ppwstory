from django.urls import path
from . import views

app_name = 'story5'

urlpatterns = [
    path('matakuliah/', views.matakuliah, name='matakuliah'),
    path('savematakuliah/', views.savematakuliah, name='savematakuliah'),
    path('matakuliah/<int:pk>/', views.getmatkul, name='getmatkul'),
    path('matakuliah/<int:pk>/removematkul', views.removematkul, name='removematkul'),

]