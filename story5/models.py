from django.db import models
#import uuid

# Create your models here.
class Course(models.Model):
    name = models.CharField(max_length = 200)
    lecturer = models.CharField(max_length = 300)
    sks = models.IntegerField()
    description = models.TextField()
    year = models.CharField(max_length = 100)
    room = models.CharField(max_length = 100, default='none')

