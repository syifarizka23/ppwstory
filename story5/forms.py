from django import forms
from .models import Course
from django.core.exceptions import ValidationError 
from django.utils.translation import ugettext_lazy as _

class Input_Form(forms.ModelForm):
    class Meta:
        model = Course
        fields = ['name', 'lecturer', 'sks', 'description', 'year', 'room']
    error_messages = {
        'required' : 'please type'
    }
    name_attrs = {
        'type' : 'text',
        'placeholder' : 'nama mata kuliah'
    }
    lecturer_attrs = {
        'type' : 'text',
        'placeholder' : 'nama dosen'
    }
    sks_attrs = {
        'type' : 'number',
        'placeholder' : 'sks'
    }
    description_attrs = {
        'type' : 'text',
        'placeholder' : 'deskripsi mata kuliah'
    }
    year_attrs = {
        'type' : 'text',
        'placeholder' : 'semester tahun'
    }
    room_attrs = {
        'type' : 'text',
        'placeholder' : 'ruangan'
    }

    name = forms.CharField(label='Mata Kuliah:', required=True, max_length=200, widget=forms.TextInput(attrs=name_attrs))
    lecturer = forms.CharField(label='Dosen', required=True, max_length=300, widget=forms.TextInput(attrs=lecturer_attrs))
    sks = forms.IntegerField(label='Jumlah SKS', required=True, widget=forms.TextInput(attrs=sks_attrs))
    description = forms.CharField(label='Deskripsi', required=False, widget=forms.Textarea(attrs=description_attrs))
    year = forms.CharField(label='Tahun Ajaran', required=True, max_length=100, widget=forms.TextInput(attrs=year_attrs))
    room = forms.CharField(label='Ruang', required=True, max_length=100, widget=forms.TextInput(attrs=room_attrs))

    def clean_sks(self):
        data = self.cleaned_data['sks']
        if data < 1:
            raise ValidationError(_('SKS tidak boleh 0 atau negatif'))
        if data > 24:
            raise ValidationError(_('SKS melebihi batas maksimum'))
        return data

