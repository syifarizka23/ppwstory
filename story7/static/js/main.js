$(document).ready(function(){
    $('.accordion-list > li > .answer').hide();

    $('.down').click(function(){
      var $parent = $(this).parents("li");
      $parent.insertAfter($parent.next()); 
      return false;
    });

    $(".up").click(function () {
      var $parent = $(this).parents("li");
      $parent.insertBefore($parent.prev()); 
      return false;
    });

    $('.accordion-list > li').click(function() { //ini buat nutup
      if ($(this).hasClass("active")) {
        $(this).removeClass("active").find(".answer").slideUp();
      } else {                                                      // ini buat buka
        $(".accordion-list > li.active .answer").slideUp();
        $(".accordion-list > li.active").removeClass("active");
        $(this).addClass("active").find(".answer").slideDown();
      }
      return false;
    });   
});