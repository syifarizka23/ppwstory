from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index

# Create your tests here.
class Story7Test(TestCase):
    def test_story7_url(self):
        response = Client().get('/story7/')
        self.assertEqual(response.status_code, 200)

    def test_using_story7_template(self):
        response = Client().get('/story7/')
        self.assertTemplateUsed(response, 'story7/accordionpage.html')

    def test_using_index_function(self):
        found = resolve('/story7/')
        self.assertEqual(found.func, index)
