from django.test import TestCase
import json

# Create your tests here.
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index

# Create your tests here.
class Story8Test(TestCase):
    def test_story8_url(self):
        response = Client().get('/story8/')
        self.assertEqual(response.status_code, 200)

    def test_using_story8_template(self):
        response = Client().get('/story8/')
        self.assertTemplateUsed(response, 'story8/index8.html')

    def test_using_index_function(self):
        found = resolve('/story8/')
        self.assertEqual(found.func, index)

    def test_get_json(self):
        response = Client().get('/story8/judul/?q=a')
        data = json.loads(response.content)
        self.assertNotEquals(data,{'totalItems':0})
