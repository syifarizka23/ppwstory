from django.shortcuts import render
from django.http import JsonResponse
import requests
import json

# Create your views here.
def index(request):
    return render(request, 'story8/index8.html')
    
def judul(request):
    arg = request.GET['q']
    url_tujuan = 'https://www.googleapis.com/books/v1/volumes?q='+arg
    getreq = requests.get(url_tujuan)
    data = json.loads(getreq.content)
    return JsonResponse(data, safe=False)