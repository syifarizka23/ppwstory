from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from .models import Kegiatan, Peserta
from .forms import Input_Form, Peserta_Form
from django.urls import reverse

# Create your views here.
def kegiatan(request):
    kegiatan = Kegiatan.objects.all()
    peserta = Peserta.objects.all()
    response = {'kegiatan' : kegiatan, 'peserta' : peserta, 'input_form' : Input_Form, 'peserta_form' : Peserta_Form}
    return render(request, 'story6/kegiatan.html', response)


def savekegiatan(request):
    form = Input_Form(request.POST or None)
    if (form.is_valid() and request.method == 'POST'):
        new_kegiatan = Kegiatan(name=form.cleaned_data['name'])
        new_kegiatan.save()
        return HttpResponseRedirect('/story6/kegiatan/')
    else:
        form = Input_Form() 
        return render(request,'story6/kegiatan.html')

def savepeserta(request, pk):
    form = Peserta_Form(request.POST or None)
    if (form.is_valid() and request.method == 'POST'):
        kegiatan_in = get_object_or_404(Kegiatan, pk=pk)
        new_peserta = Peserta()
        new_peserta.name = form.cleaned_data['name']
        new_peserta.kegiatan = kegiatan_in
        new_peserta.save()
        return HttpResponseRedirect('/story6/kegiatan/')
    else:
        form = Peserta_Form()
        return render(request,'story6/kegiatan.html')

