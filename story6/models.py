from django.db import models

# Create your models here.
class Kegiatan(models.Model):
    name = models.CharField(max_length=125, default='none')

class Peserta(models.Model):
    #aktivitas = models.ForeignKey(Kegiatan, on_delete = models.CASCADE)
    name = models.CharField(max_length=125, default='none')
    kegiatan = models.ForeignKey(Kegiatan, on_delete=models.DO_NOTHING, default='none', null=True, blank=True)

    class Meta:
        ordering = ['name']
