from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import kegiatan, savekegiatan, savepeserta
from .models import Kegiatan, Peserta
from .forms import Input_Form, Peserta_Form


# # Create your tests here.
class TestStory6(TestCase):

    def test_url_response(self):
        response = Client().get('/story6/kegiatan/')
        self.assertEqual(response.status_code, 200)
    
    def test_template_story6(self):
        response = Client().get('/story6/kegiatan/')
        self.assertTemplateNotUsed(response, 'kegiatan.html')


    def test_view_formulir_story6(self):
        response = Client().get('/story6/kegiatan/')
        isi = response.content.decode('utf8')

        self.assertIn('<form action="/story6/savekegiatan/" method="POST">', isi)
        # lanjutin

    def test_model_kegiatan_story6(self):
        Kegiatan.objects.create(name="PPW")
        data = Kegiatan.objects.all().count()
        self.assertEquals(data, 1)

    def test_model_peserta_story6(self):
        kegiatan = Kegiatan.objects.create(name="PPW")
        peserta = Peserta.objects.create(name="aku", kegiatan=kegiatan)
        data = Peserta.objects.filter(name = "aku").count()
        self.assertEquals(data, 1)

    def test_kirim_post_kegiatan(self):
        arg = {'name': 'coba'}

        response = Client().post('/story6/savekegiatan/', arg)
        # isi = response.content.decode('utf8')
        # self.assertIn('coba' , isi)
        self.assertEqual(response.status_code, 302)
    
    def test_kirim_post_peserta(self):
        arg = {'name': 'coba1'}
        kegiatan = Kegiatan.objects.create(name="PPW")
        response = Client().post('/story6/savepeserta/'+str(kegiatan.id), arg)
        # isi = response.content.decode('utf8')
        self.assertEqual(response.status_code, 302)
        # self.assertIn('coba1' , isi)
    





# class Story6UnitTest(TestCase):
    
#     def test_story6_url_is_exist(self):
#         response = Client().get('/story6/kegiatan')
#         self.assertEqual(response.status_code, 200)
    
#     def test_story6_using_index_func(self):
#         found = resolve('/story6/kegiatan')
#         self.assertEqual(found.func, index)
    
#     def test_model_can_create_new_kegiatan(self):
#         # Creating a new activity
#         new_activity = Kegiatan.objects.create(name='kegiatan 1')
    
#         # Retrieving all available activity
#         counting_all_available_kegiatan = Kegiatan.objects.all().count()
#         self.assertEqual(counting_all_available_kegiatan, 1)
    
#     # def test_form_kegiatan_input_has_placeholder_and_css_classes(self):
#     #     form = Todo_Form()
#     #     self.assertIn('class="todo-form-input', form.as_p())
#     #     self.assertIn('id="id_title"', form.as_p())
#     #     self.assertIn('class="todo-form-textarea', form.as_p())
#     #     self.assertIn('id="id_description', form.as_p())
    
#     # def test_form_validation_for_blank_items(self):
#     #     form = Todo_Form(data={'title': '', 'description': ''})
#     #     self.assertFalse(form.is_valid())
#     #     self.assertEqual(form.errors['description'],["This field is required."])

#     def test_lab5_post_success_and_render_the_result(self):
#         test = 'Anonymous'
#         response_post = Client().post('/lab-5/add_todo', {'title': test, 'description': test})
#         self.assertEqual(response_post.status_code, 302)
    
#         response= Client().get('/lab-5/')
#         html_response = response.content.decode('utf8')
#         self.assertIn(test, html_response)
    
#     def test_lab5_post_error_and_render_the_result(self):
#         test = 'Anonymous'
#         response_post = Client().post('/lab-5/add_todo', {'title': '', 'description': ''})
#         self.assertEqual(response_post.status_code, 302)
    
#         response= Client().get('/lab-5/')
#         html_response = response.content.decode('utf8')
#         self.assertNotIn(test, html_response)

