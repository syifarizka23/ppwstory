from django import forms
from .models import Kegiatan, Peserta
# from django.core.exceptions import ValidationError 
# from django.utils.translation import ugettext_lazy as _

class Input_Form(forms.ModelForm):
    class Meta:
        model = Kegiatan
        fields = ['name']
    error_messages = {
        'required' : 'please type'
    }
    name_attrs = {
        'type' : 'text',
        'placeholder' : 'kegiatan'
    }


    name = forms.CharField(label='kegiatan:', required=True, max_length=125, widget=forms.TextInput(attrs=name_attrs))

class Peserta_Form(forms.ModelForm):
    class Meta:
        model = Peserta
        fields = ['name']
    error_messages = {
        'required' : 'please type'
    }
    name_attrs = {
        'type' : 'text',
        'placeholder' : 'nama'
    }


    name = forms.CharField(label='', required=True, max_length=125, widget=forms.TextInput(attrs=name_attrs))