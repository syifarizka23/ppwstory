from django.urls import path
from . import views

app_name = 'story6'

urlpatterns = [
    path('kegiatan/', views.kegiatan, name='kegiatan'),
    path('savekegiatan/', views.savekegiatan, name='savekegiatan'),
    path('savepeserta/<int:pk>', views.savepeserta, name='savepeserta'),
]