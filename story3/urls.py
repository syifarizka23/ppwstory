from django.urls import path
from . import views

app_name = 'story3'

urlpatterns = [
    path('expirience/', views.profile, name='profile'),
    path('contact/', views.contact, name='contact'),
]